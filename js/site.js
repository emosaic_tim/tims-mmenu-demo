$(document).ready(function(){
    $("#my-mmenu").mmenu({
        "extensions": [
            "shadow-page",
            "shadow-panels"
         ],
         "offCanvas": {
            "position": "right"
        },
        "iconPanels": true,
        "fixedElements": {
            "fixed": "header"
        },
        "backButton": {
            "close": true
        },
        
    },
    {
        "clone": true
    });
});